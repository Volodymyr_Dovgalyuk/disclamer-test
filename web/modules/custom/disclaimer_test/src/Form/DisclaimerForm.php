<?php
/**
 * @file
 * Contains \Drupal\disclaimer_test\Form\DisclaimerForm.
 */

namespace Drupal\disclaimer_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form with modal window.
 */
class DisclaimerForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'disclaimer_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $print_data = NULL) {
    $form['#id'] = 'disclaimer-form';

    $form['disclaimer_text'] = [
      '#markup' => $print_data,
    ];
    $form['accept'] = [
      '#type'  => 'submit',
      '#value' => t('Accept'),
      '#attributes' => [
        'id' => 'accept'
      ],
    ];
    $form['deny'] = [
      '#type'       => 'button',
      '#value'      => ('Decline'),
      '#attributes' => [
        'id' => 'decline'
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}