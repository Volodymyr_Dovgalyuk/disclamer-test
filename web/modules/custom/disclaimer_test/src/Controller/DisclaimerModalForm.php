<?php
/**
 * @file
 * Contains \Drupal\disclaimer_test\Controller\DisclaimerModalForm.
 */

namespace Drupal\disclaimer_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\disclaimer_test\Form\DisclaimerForm;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Response;

/**
 *  DisclaimerModalForm Controller
 */
class DisclaimerModalForm extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function constructor() {
    if (isset($_POST['id'])) {
      $disclaimer_id = $_POST['id'];
      $disclaimer_node = Node::load($disclaimer_id);
      $print_data = $disclaimer_node->get('body')->getValue()[0]['value'];
    }
    else {
      $print_data = '';
    }
    $form_main = \Drupal::formBuilder()->getForm(DisclaimerForm::class, $print_data);
    $form_main['#action'] = '';
    $rendered = \Drupal::service('renderer')->render($form_main);
    $response = new Response($rendered, Response::HTTP_OK, ['content-type' => 'text/html']);
    return $response;
  }
}




