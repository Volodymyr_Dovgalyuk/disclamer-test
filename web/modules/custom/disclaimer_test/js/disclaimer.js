/**
 * @file
 * Disclaimer js implementation.
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.disclaimer_test = {
    attach: function (context, settings) {

      $.each(drupalSettings.disclaimer_data, function (index, value) {
        let nid = $(this)[0].nid;
        if ($.cookie(index) !== nid) {
          $.post('/modal_disclaimer', {id: nid}, function (result) {
            $('<div>', context).dialog({
              closeOnEscape: false,
              open: function (event, ui) {
                $('.ui-dialog-titlebar-close').hide();
                $('.ui-dialog-title').html(Drupal.t('Important information'));
                $('.ui-dialog-content').html(result);

                $('#accept').unbind('click').bind('click', function () {
                  let date = new Date(new Date().getTime() + 31556926);
                  $.cookie(index, nid, {dates: date});
                });
                $('#decline').unbind('click').bind('click', function () {
                  window.location.href = '/';
                  return false;
                });
              },
              resizable: false,
              height: 'auto',
              width: '60%',
              modal: true,
            });
          });
        }
      });

    }
  };
})(jQuery, Drupal, drupalSettings);